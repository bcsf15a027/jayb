package com.example.trickzzi.dms.JyabActivity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.trickzzi.dms.Model.FirebaseHandler;
import com.example.trickzzi.dms.Model.MoneyTransfer;
import com.example.trickzzi.dms.Model.User;
import com.example.trickzzi.dms.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Distributer extends AppCompatActivity
{

    private Button request;
    private CheckBox checkBox;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference, dref;
    private ProgressDialog progressDialog;
    private List<User> userslist = new ArrayList<>();
    private ValueEventListener valueEventListener;
    private EditText edittxt_amount, edittxt_desc;
    private ArrayList<String> Userskeys = new ArrayList<>();
    private ArrayList<String> CheckedBox = new ArrayList<>();
    private LinearLayout LL_checkbox;
    private TextView wronginput;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributer);

        edittxt_amount = findViewById(R.id.edittxt_amount);
        edittxt_desc = findViewById(R.id.edittxt_desc);
        LL_checkbox = findViewById(R.id.LL_checkbox);
        wronginput=findViewById(R.id.wrongamount);
        request = findViewById(R.id.btn_Request);
       // wronginput.setVisibility(View.GONE);


        SharedPreferences sharedPreferences = getSharedPreferences("SavePassword", MODE_PRIVATE);
        final String user_id = sharedPreferences.getString("LoggedInUserID", "null");

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userslist.clear();
                CreateCheckBox();
                float count = dataSnapshot.getChildrenCount();
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    User user = obj.getValue(User.class);
                    Userskeys.add(obj.getKey());
                    userslist.add(user);
                }
                CreateCheckBox();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        request.setOnClickListener(
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                            CheckedBox.clear();
                            for (int i = 0; i < LL_checkbox.getChildCount(); i++) {
                                View nextChild = LL_checkbox.getChildAt(i);
                                if (nextChild instanceof CheckBox) {
                                    CheckBox check = (CheckBox) nextChild;
                                    if (check.isChecked()) {
                                        CheckedBox.add(Integer.toString(check.getId()));
                                    }
                                }
                            }
                            if (!(CheckedBox.size() == 0)) {
                                FirebaseHandler obj = new FirebaseHandler();
                                if (!edittxt_desc.getText().toString().isEmpty() && !edittxt_amount.getText().toString().isEmpty()) {
                                    progressDialog = new ProgressDialog(Distributer.this);
                                    progressDialog.setTitle("Send Money Request");
                                    progressDialog.setMessage("Requesting...");
                                    progressDialog.setCancelable(false);
                                    progressDialog.show();


                                    double share = 0;
                                    double amt = 0;
                                    int RequesterID = 0;
                                    boolean inclueedItself = false;
                                    for (int i = 0; i < CheckedBox.size(); i++) {
                                        String checkboxid = CheckedBox.get(i);
                                        if (TextUtils.equals(checkboxid, user_id)) {
                                            inclueedItself = true;
                                        }
                                        if (!TextUtils.equals(checkboxid, user_id)) {

                                            dref = FirebaseDatabase.getInstance().getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/money_transfer");
                                            String Unikey = dref.push().getKey();
                                            MoneyTransfer sendobj = new MoneyTransfer();
                                            sendobj.setCat_id("1");
                                            sendobj.setFrom_user_id(CheckedBox.get(i));
                                            amt = Double.parseDouble(edittxt_amount.getText().toString());
                                            share = amt / (CheckedBox.size());
                                            //round2(share,10);
                                            //round2(amt,10);
                                            sendobj.setMt_amount(Double.toString(share));
                                            sendobj.setMt_desc(edittxt_desc.getText().toString());
                                            sendobj.setMt_id("3dj3");
                                            sendobj.setMt_status("topay");
                                            sendobj.setObjection(" ");
                                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd-MM-yyyy");
                                            String currentDateandTime = sdf.format(new Date());
                                            sendobj.setRequest_time(currentDateandTime);
                                            sendobj.setTo_user_id(user_id);
                                            dref.child(Unikey).setValue(sendobj).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    progressDialog.dismiss();
                                                    Toast.makeText(Distributer.this, "Data Saved Sucessfully", Toast.LENGTH_SHORT).show();

                                                }
                                            });
                                            //int VicitimID = Integer.parseInt(CheckedBox.get(i)) - 1;
                                            //RequesterID = Integer.parseInt(user_id) - 1;
                                            //obj.addPayableOfVictim(user_id, CheckedBox.get(i), share, userslist.get(VicitimID).getPayable());
                                        }
                                    }


                                    if (inclueedItself) {
                                        amt = amt - share;
                                    }
                                    //obj.addRecieableOFRequester(user_id, amt, userslist.get(RequesterID).getReceivable());
                                    edittxt_amount.setText("");
                                    edittxt_desc.setText("");
                                }
                            } else {
                                Toast.makeText(Distributer.this, "Select User !", Toast.LENGTH_SHORT).show();


                            }
                    }
                });
    }

    public void CreateCheckBox() {
        if (userslist.size() == 0) {
            LL_checkbox.removeAllViewsInLayout();
        }
        for (int i = 0; i < userslist.size(); i++) {
            checkBox = new CheckBox(Distributer.this);
            checkBox.setId(Integer.parseInt(userslist.get(i).getUser_id()));
            checkBox.setText(userslist.get(i).getFull_name());
            checkBox.setOnClickListener(getOnClickDoSomething(checkBox));
            LL_checkbox.addView(checkBox);
        }
    }

    View.OnClickListener getOnClickDoSomething(final Button button) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            }
        };
    }

    private class AddTranscation extends AsyncTask<Void, Void, Void>
    {
        private String user_id;
        public AddTranscation(String user_id)
        {
            super();
            this.user_id=user_id;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            // Showing progress dialog


        }

        @Override
        protected Void doInBackground(Void... arg0)
        {
            CheckedBox.clear();
            for (int i = 0; i < LL_checkbox.getChildCount(); i++)
            {
                View nextChild = LL_checkbox.getChildAt(i);
                if (nextChild instanceof CheckBox) {
                    CheckBox check = (CheckBox) nextChild;
                    if (check.isChecked()) {
                        CheckedBox.add(Integer.toString(check.getId()));
                    }
                }
            }
            FirebaseHandler obj = new FirebaseHandler();
            if (!edittxt_desc.getText().toString().isEmpty() && !edittxt_amount.getText().toString().isEmpty())
            {
                double share = 0;
                double amt=0;
                int RequesterID=0;
                boolean inclueedItself=false;

                for (int i = 0; i < CheckedBox.size(); i++)
                {
                    try
                    {
                        String checkboxid = CheckedBox.get(i);
                        if (TextUtils.equals(checkboxid, user_id))
                        {
                            inclueedItself=true;
                        }
                        if (!TextUtils.equals(checkboxid, user_id))
                        {

                            dref = FirebaseDatabase.getInstance().getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/money_transfer");
                            String Unikey = dref.push().getKey();
                            dref.child(Unikey).child("cat_id").setValue("1");
                            dref.child(Unikey).child("from_user_id").setValue(CheckedBox.get(i));
                            amt = Double.parseDouble(edittxt_amount.getText().toString());
                            share = amt / (CheckedBox.size());

                            dref.child(Unikey).child("mt_amount").setValue(Double.toString(share));
                            dref.child(Unikey).child("mt_desc").setValue(edittxt_desc.getText().toString());
                            dref.child(Unikey).child("mt_id").setValue("3dj3");
                            dref.child(Unikey).child("mt_status").setValue("topay");
                            dref.child(Unikey).child("objection").setValue(" ");
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd-MM-yyyy");
                            String currentDateandTime = sdf.format(new Date());
                            dref.child(Unikey).child("request_time").setValue(currentDateandTime);
                            if(CheckedBox.size()-1==i)
                            {

                                dref.child(Unikey).child("to_user_id").setValue(user_id, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                        if (databaseError == null)
                                        {
                                            //progressDialog.dismiss();
                                            runOnUiThread(new Runnable()
                                            {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext()
                                                            , "Data Saved Sucessfully", Toast.LENGTH_SHORT).show();

                                                }
                                            });

                                        }
                                        if (databaseError != null)
                                        {
                                            runOnUiThread(new Runnable()
                                            {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(), "Error !", Toast.LENGTH_SHORT).show();

                                                }
                                            });
                                        }
                                    }
                                });
                            }
                            else
                            {
                                dref.child(Unikey).child("to_user_id").setValue(user_id);
                            }


                            int VicitimID = Integer.parseInt(CheckedBox.get(i)) - 1;
                            RequesterID = Integer.parseInt(user_id) - 1;
                            obj.addPayableOfVictim(user_id, CheckedBox.get(i), share, userslist.get(VicitimID).getPayable());
                        }

                    }
                    catch (Exception e)
                    {
                        //Toast.makeText(Distributer.this, "Go back and Check !", Toast.LENGTH_SHORT).show();
                    }

                }
                if(inclueedItself)
                {
                    amt=amt-share;
                }
                obj.addRecieableOFRequester(user_id,amt,userslist.get(RequesterID).getReceivable());
                edittxt_amount.setText("");
                edittxt_desc.setText("");
            }


            runOnUiThread(new Runnable()
            {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),
                            "Couldn't get json from server. Check LogCat for possible errors!",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }

    }

    public static double round2(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }







}
