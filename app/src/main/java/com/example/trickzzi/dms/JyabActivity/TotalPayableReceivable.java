package com.example.trickzzi.dms.JyabActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.Toast;
import com.example.trickzzi.dms.Adapters.PayableAdapter;
import com.example.trickzzi.dms.Adapters.PayableTotalAdapter;
import com.example.trickzzi.dms.Extra.MyDividerItemDecoration;
import com.example.trickzzi.dms.Model.MoneyTransfer;
import com.example.trickzzi.dms.Model.TotalMoney;
import com.example.trickzzi.dms.Model.User;
import com.example.trickzzi.dms.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;

public class TotalPayableReceivable extends AppCompatActivity
{

    private List<User> userslist = new ArrayList<>();
    private List<TotalMoney> totalMoneyList=new ArrayList<>();

    private PayableTotalAdapter payableTotalAdapter;
    private ProgressDialog progressDialog;

    private RecyclerView totalPayableRV;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    ValueEventListener valueEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_payable_receivable);

        SharedPreferences sharedPreferences=getSharedPreferences("SavePassword",MODE_PRIVATE);
        final String user_id = sharedPreferences.getString("LoggedInUserID","null");

        progressDialog = new ProgressDialog(TotalPayableReceivable.this);
        progressDialog.setMessage("Loading");
        progressDialog.setCancelable(false);
        progressDialog.show();

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");

        databaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                userslist.clear();
                totalMoneyList.clear();
                float count = dataSnapshot.getChildrenCount();
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    User user = obj.getValue(User.class);
                    if(!TextUtils.equals(user.getUser_id(),user_id))
                    {
                        getTotalPayable(user_id,user.getUser_id(),user.getFull_name());
                    }

                    userslist.add(user);
                }
                progressDialog.dismiss();


            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
            //getTotalPayable(user_id,userslist.get(index++).getUser_id(),index,userslist.get(index++).getFull_name());
            //getTotalPayable(user_id,"3","Zeeshan Tariq");
            //getTotalPayable(user_id,"4","Kashif Nawaz");
            //getTotalPayable(user_id,"5","Fahad");



        totalPayableRV = findViewById(R.id.payable_Recycler_view_total);
        totalPayableRV.setLayoutManager(new LinearLayoutManager(TotalPayableReceivable.this));
        payableTotalAdapter= new PayableTotalAdapter(totalMoneyList);
        RecyclerView.LayoutManager mTLayoutManager = new LinearLayoutManager(getApplicationContext());
        totalPayableRV.setLayoutManager(mTLayoutManager);
        totalPayableRV.setItemAnimator(new DefaultItemAnimator());
        totalPayableRV.setAdapter(payableTotalAdapter);
        totalPayableRV.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        payableTotalAdapter.notifyDataSetChanged();


    }
    public void getTotalPayable(final String user_id, final String to_user_id,final String fullname)
    {

        Query q=FirebaseDatabase.getInstance().getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/").
                child("money_transfer").orderByChild("from_user_id").equalTo(user_id);
        valueEventListener=new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                double payable = 0;
                double totalpayable=0;
                String data="";
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    MoneyTransfer mt_obj = obj.getValue(MoneyTransfer.class);
                    payable=0;
                    if (TextUtils.equals(mt_obj.getTo_user_id(),to_user_id))
                    {
                        payable = Double.parseDouble(mt_obj.getMt_amount());
                        totalpayable += payable;
                    }
                }
                TotalMoney totalMoney = new TotalMoney();
                totalMoney.setName(fullname);
                totalMoney.setTotal(totalpayable);
                totalMoney.setUser_id(user_id);
                data =" " + totalpayable + " |";
                totalMoneyList.add(totalMoney);
                //Toast.makeText(TotalPayableReceivable.this,data,Toast.LENGTH_LONG).show();
               payableTotalAdapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        };
        q.addListenerForSingleValueEvent(valueEventListener);
    }
}
