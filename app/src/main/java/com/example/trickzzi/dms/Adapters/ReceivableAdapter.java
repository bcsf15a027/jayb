package com.example.trickzzi.dms.Adapters;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.trickzzi.dms.Model.MoneyTransfer;
import com.example.trickzzi.dms.Model.User;
import com.example.trickzzi.dms.R;

import java.util.List;

public class ReceivableAdapter extends RecyclerView.Adapter<ReceivableAdapter.MyViewHolder>
{

    Context context;

    private List<MoneyTransfer> moneyTransferlist;
    private List<User> userslist;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView desc,from,amount,date;
        private ImageView edit,cancel;

        public MyViewHolder(View view)
        {
            super(view);
            desc = (TextView) view.findViewById(R.id.txt_rec_desc);
            from = (TextView) view.findViewById(R.id.txt_rc_from);
            amount = (TextView) view.findViewById(R.id.txt_rec_amount);
            date=(TextView)view.findViewById(R.id.txt_rec_date);

            //edit=(ImageView) view.findViewById(R.id.rec_edit_btn);
            //cancel=(ImageView) view.findViewById(R.id.rec_cancel_btn);



        }
    }


    public ReceivableAdapter(List<MoneyTransfer> moviesList,List<User> userlist,Context c)
    {
        this.moneyTransferlist = moviesList;
        this.userslist=userlist;
        this.context=c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.receivable_list_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        MoneyTransfer moneyTransfer = moneyTransferlist.get(position);
        holder.desc.setText(moneyTransfer.getMt_desc());

        int index=Integer.parseInt(moneyTransfer.getFrom_user_id())-1;
        holder.from.setText(userslist.get(index).getFull_name());

        holder.amount.setText(moneyTransfer.getMt_amount());
        holder.date.setText(moneyTransfer.getRequest_time());
    }

    @Override
    public int getItemCount() {
        return moneyTransferlist.size();
    }
}