package com.example.trickzzi.dms.JyabActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.List;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.trickzzi.dms.Model.User;
import com.example.trickzzi.dms.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import android.app.ProgressDialog;


public class MainActivity extends AppCompatActivity
{



    private EditText username,password;
    private TextView wrongid,data;
    private Button signin;
    private int position=0;
    private CheckBox savepassword;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private ValueEventListener valueEventListener;

    private List<User> userslist = new ArrayList<>();
    private ArrayList<String> keys=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences=getSharedPreferences("SavePassword",MODE_PRIVATE);
        if(sharedPreferences.getBoolean("LoggedIn",false))
        {
            Intent intent = new Intent(MainActivity.this, UserDashBoard.class);
            startActivity(intent);

        }
        username=findViewById(R.id.username);
        password=findViewById(R.id.password);
        signin=findViewById(R.id.signin);
        data=findViewById(R.id.data1);

        savepassword=findViewById(R.id.savepassoword);
        firebaseDatabase = FirebaseDatabase.getInstance();

        sharedPreferences=getSharedPreferences("SavePassword",MODE_PRIVATE);
        username.setText(sharedPreferences.getString("username",""));
        password.setText(sharedPreferences.getString("password",""));


        }

    public void onSignIN(View v)
    {
        final String User1=username.getText().toString();
        final String Pass2=password.getText().toString();

        if(savepassword.isChecked())
        {
            SharedPreferences sharedPreferences=getSharedPreferences("SavePassword",MODE_PRIVATE);
            SharedPreferences.Editor editor=sharedPreferences.edit();
            editor.putString("username",User1);
            editor.putString("password",Pass2);
            editor.commit();
        }

        data.setText("");
        final ProgressDialog p=new ProgressDialog(MainActivity.this);
        p.setTitle("DMS");
        p.setMessage("Loading..");
        p.setCancelable(false);
        p.show();


        Query q=FirebaseDatabase.getInstance().getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/").
                child("users").orderByChild("user_name").equalTo(User1);

        valueEventListener=new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {

                userslist.clear();
                keys.clear();

                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    keys.add(obj.getKey());
                    User userobj = obj.getValue(User.class);
                    userslist.add(userobj);

                    if(userobj.getPassword().equals(Pass2))
                    {
                        SharedPreferences sharedPreferences=getSharedPreferences("SavePassword",MODE_PRIVATE);
                        SharedPreferences.Editor editor=sharedPreferences.edit();
                        editor.putString("LoggedInUserID",userobj.getUser_id());
                        editor.putString("IsAdmin",userobj.getIsAdmin());
                        editor.putBoolean("LoggedIn",true);
                        editor.commit();

                        Intent intent = new Intent(MainActivity.this, UserDashBoard.class);
                        //intent.putExtra("user_id", userobj.getUser_id());
                        //intent.putExtra("user_key", keys.get(0));
                        p.dismiss();
                        startActivity(intent);
                    }
                    else
                    {
                        p.dismiss();
                        data.setText("Incorrect Passowrd");
                    }
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Toast.makeText(MainActivity.this, "Error Occourd!",Toast.LENGTH_LONG).show();

            }
        };
        q.addListenerForSingleValueEvent(valueEventListener);

    }
}
