package com.example.trickzzi.dms.JyabActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.trickzzi.dms.Model.FirebaseHandler;
import com.example.trickzzi.dms.Model.User;
import com.example.trickzzi.dms.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ModernDashboard extends AppCompatActivity
{
    private TextView name, bal, payable, rec;
    private Button signout,btn_signout_dashboard;
    private LinearLayout LL_payable, LL_rec, LL_bal, LL_checkbox;
    private CheckBox checkBox;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference,dref;
    private List<User> userslist = new ArrayList<>();
    private ValueEventListener valueEventListener;
    private EditText edittxt_amount,edittxt_desc;
    private ArrayList<String> Userskeys = new ArrayList<>();
    private ArrayList<String> CheckedBox = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modern_dashboard);

        bal = findViewById(R.id.txt_balance);
        signout = findViewById(R.id.btn_signout);
        name = findViewById(R.id.txt_username);
        payable = findViewById(R.id.txt_payable);
        rec = findViewById(R.id.txt_receivable);
        edittxt_amount=findViewById(R.id.edittxt_amount);
        edittxt_desc=findViewById(R.id.edittxt_desc);
        //btn_signout_dashboard=findViewById(R.id.btn_signout_dashboard);

        LL_payable = findViewById(R.id.LL_payable);
        LL_bal = findViewById(R.id.LL_balance);
        LL_rec = findViewById(R.id.LL_receiable);
        LL_checkbox = findViewById(R.id.LL_checkbox);

        SharedPreferences sharedPreferences=getSharedPreferences("SavePassword",MODE_PRIVATE);
        final String user_id = sharedPreferences.getString("LoggedInUserID","null");

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");
        databaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                userslist.clear();
                CreateCheckBox();
                float count = dataSnapshot.getChildrenCount();
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    User user = obj.getValue(User.class);
                    Userskeys.add(obj.getKey());
                    userslist.add(user);
                }
                UpdateInfo(user_id);
                CreateCheckBox();
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

        /*btn_signout_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                SharedPreferences sharedPreferences=getSharedPreferences("SavePassword",MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("LoggedInUserID","null");
                editor.putBoolean("LoggedIn",false);
                editor.commit();
                Intent intent = new Intent(ModernDashboard.this, MainActivity.class);
                startActivity(intent);


            }
        });*/


        LL_payable.setOnClickListener(
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ModernDashboard.this, Payable.class);
                        intent.putExtra("user_id", user_id);
                        startActivity(intent);

                    }
                });
        LL_rec.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ModernDashboard.this, Receivable.class);
                        intent.putExtra("user_id", user_id);
                        startActivity(intent);

                    }
                });
        signout.setOnClickListener(
                new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CheckedBox.clear();

                String checkstr="";
                for(int i=0; i<LL_checkbox.getChildCount(); i++)
                {
                    View nextChild = LL_checkbox.getChildAt(i);
                    if(nextChild instanceof CheckBox)
                    {
                        CheckBox check = (CheckBox) nextChild;
                        if (check.isChecked())
                        {
                            //checkstr=checkstr+", "+Integer.toString(check.getId());
                            CheckedBox.add(Integer.toString(check.getId()));
                        }
                    }
                }

                //Toast.makeText(ModernDashboard.this,checkstr,Toast.LENGTH_LONG).show();
                if (!edittxt_desc.getText().toString().isEmpty() && !edittxt_amount.getText().toString().isEmpty())
                {
                for(int i=0;i<CheckedBox.size();i++)
                {

                        dref = FirebaseDatabase.getInstance().getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/money_transfer");
                        String Unikey = dref.push().getKey();
                        dref.child(Unikey).child("cat_id").setValue("1", new DatabaseReference.CompletionListener()
                        {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference)
                            {
                                if (databaseError == null) {
                                    Toast.makeText(ModernDashboard.this, "Data Saved Sucessfully", Toast.LENGTH_SHORT).show();
                                }
                                if (databaseError != null)
                                {
                                    Toast.makeText(ModernDashboard.this, "Error !", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        dref.child(Unikey).child("from_user_id").setValue(CheckedBox.get(i));
                        double amt=Double.parseDouble(edittxt_amount.getText().toString());
                        double share=amt/(CheckedBox.size());
                        dref.child(Unikey).child("mt_amount").setValue(Double.toString(share));
                        dref.child(Unikey).child("mt_desc").setValue(edittxt_desc.getText().toString());
                        dref.child(Unikey).child("mt_id").setValue("3dj3");
                        dref.child(Unikey).child("mt_status").setValue("topay");
                        dref.child(Unikey).child("objection").setValue(" ");
                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd-MM-yyyy");
                        String currentDateandTime = sdf.format(new Date());
                        dref.child(Unikey).child("request_time").setValue(currentDateandTime);
                        dref.child(Unikey).child("to_user_id").setValue(user_id);
                        FirebaseHandler obj=new FirebaseHandler();

                    }
                    edittxt_amount.setText("");
                    edittxt_desc.setText("");
                }
            }
        });


    }
    public void UpdateInfo(final String User1)
    {
        Query q=FirebaseDatabase.getInstance().getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/").
                child("users").orderByChild("user_id").equalTo(User1);
        valueEventListener=new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    User userobj = obj.getValue(User.class);
                    name.setText(userobj.getFull_name());
                    bal.setText(userobj.getBalance());
                    payable.setText(userobj.getPayable());
                    rec.setText(userobj.getReceivable());
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Toast.makeText(ModernDashboard.this, "Error Occourd!",Toast.LENGTH_LONG).show();
            }
        };
        q.addListenerForSingleValueEvent(valueEventListener);
    }

    public void CreateCheckBox()
    {
        if(userslist.size()==0)
        {
            LL_checkbox.removeAllViewsInLayout();
        }
        for (int i = 0; i < userslist.size(); i++)
        {
            checkBox = new CheckBox(ModernDashboard.this);
            checkBox.setId(Integer.parseInt(userslist.get(i).getUser_id()));
            checkBox.setText(userslist.get(i).getFull_name());
            checkBox.setOnClickListener(getOnClickDoSomething(checkBox));
            LL_checkbox.addView(checkBox);
        }
    }

    View.OnClickListener getOnClickDoSomething(final Button button)
    {
        return new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
            }
        };
    }
}
