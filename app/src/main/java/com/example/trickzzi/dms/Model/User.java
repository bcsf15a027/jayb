package com.example.trickzzi.dms.Model;
public class User
{

    private String receivable;
    private String payable;
    private String password;
    private String balance;
    private String full_name;
    private String user_name;
    private String user_id;
    private String isAdmin;
    public User()
    {

    }
    public User(String receivable, String payable, String password, String balance, String full_name, String user_name, String user_id)
    {
        this.receivable = receivable;
        this.payable = payable;
        this.password = password;
        this.balance = balance;
        this.full_name = full_name;
        this.user_name = user_name;
        this.user_id = user_id;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public String getReceivable() {
        return receivable;
    }

    public void setReceivable(String receivable) {
        this.receivable = receivable;
    }

    public String getPayable() {
        return payable;
    }

    public void setPayable(String payable) {
        this.payable = payable;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
