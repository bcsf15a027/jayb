package com.example.trickzzi.dms.Model;

public class MoneyTransfer
{
    private String cat_id;
    private String from_user_id;
    private String mt_amount;
    private String mt_desc;
    private String mt_id;
    private String mt_status;
    private String request_time;
    private String to_user_id;
    private String objection;
    public MoneyTransfer()
    {

    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public void setMt_amount(String mt_amount) {
        this.mt_amount = mt_amount;
    }

    public void setMt_desc(String mt_desc) {
        this.mt_desc = mt_desc;
    }

    public void setMt_id(String mt_id) {
        this.mt_id = mt_id;
    }

    public void setMt_status(String mt_status) {
        this.mt_status = mt_status;
    }

    public void setRequest_time(String request_time) {
        this.request_time = request_time;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public void setObjection(String objection) {
        this.objection = objection;
    }

    public MoneyTransfer(String cat_id, String from_user_id, String mt_amount, String mt_desc, String mt_id, String mt_status, String request_time, String to_user_id, String objection) {
        this.cat_id = cat_id;
        this.from_user_id = from_user_id;
        this.mt_amount = mt_amount;
        this.mt_desc = mt_desc;
        this.mt_id = mt_id;
        this.mt_status = mt_status;
        this.request_time = request_time;
        this.to_user_id = to_user_id;
        this.objection = objection;
    }

    public String getCat_id() {
        return cat_id;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public String getMt_amount() {
        return mt_amount;
    }

    public String getMt_desc() {
        return mt_desc;
    }

    public String getMt_id() {
        return mt_id;
    }

    public String getMt_status() {
        return mt_status;
    }

    public String getRequest_time() {
        return request_time;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public String getObjection() {
        return objection;
    }
}
