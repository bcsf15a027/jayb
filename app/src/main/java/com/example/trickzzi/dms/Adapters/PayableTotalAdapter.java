package com.example.trickzzi.dms.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.trickzzi.dms.Model.MoneyTransfer;
import com.example.trickzzi.dms.Model.TotalMoney;
import com.example.trickzzi.dms.Model.User;
import com.example.trickzzi.dms.R;

import java.util.ArrayList;
import java.util.List;

public class PayableTotalAdapter extends RecyclerView.Adapter<PayableTotalAdapter.MyViewHolder>
{

    private List<TotalMoney> totalMoneyList;
    private List<User> userslist = new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView name,amount;

        public MyViewHolder(View view)
        {
            super(view);
            name = (TextView) view.findViewById(R.id.name_total_payable_lisview);
            amount = (TextView) view.findViewById(R.id.amt_total_payable_lisview);
        }
    }


    public PayableTotalAdapter(List<TotalMoney> moviesList)
    {
        this.totalMoneyList = moviesList;
    }

    @Override
    public PayableTotalAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.total_payable_listiview, parent, false);

        return new PayableTotalAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PayableTotalAdapter.MyViewHolder holder, int position)
    {
        TotalMoney totalMoney = totalMoneyList.get(position);
        holder.amount.setText("Rs. "+totalMoney.getTotal());
        holder.name.setText(totalMoney.getName());
    }

    @Override
    public int getItemCount()
    {
        return totalMoneyList.size();
    }
}