package com.example.trickzzi.dms.Adapters;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.trickzzi.dms.Model.MoneyTransfer;
import com.example.trickzzi.dms.Model.User;
import com.example.trickzzi.dms.R;

import java.util.List;

public class PayableAdapter extends RecyclerView.Adapter<PayableAdapter.MyViewHolder>
{

    private List<MoneyTransfer> moneyTransferlist;
    private List<User> userslist;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView desc, from, datetime,amount;
        public Button pay;

        public MyViewHolder(View view)
        {
            super(view);
            desc = (TextView) view.findViewById(R.id.txt_payable_desc);
            from = (TextView) view.findViewById(R.id.txt_payable_from);
            datetime = (TextView) view.findViewById(R.id.txt_payable_date);
            amount=(TextView)view.findViewById(R.id.txt_payable_amount);
        }
    }


    public PayableAdapter(List<MoneyTransfer> moviesList, List<User> userslist)
    {
        this.moneyTransferlist = moviesList;
        this.userslist=userslist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payable_list_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        MoneyTransfer moneyTransfer = moneyTransferlist.get(position);
        holder.desc.setText(moneyTransfer.getMt_desc());

        int index=Integer.parseInt(moneyTransfer.getTo_user_id())-1;
        holder.from.setText(userslist.get(index).getFull_name());
        holder.datetime.setText(moneyTransfer.getRequest_time());
        holder.amount.setText("Rs. "+moneyTransfer.getMt_amount());
    }

    @Override
    public int getItemCount() {
        return moneyTransferlist.size();
    }
}