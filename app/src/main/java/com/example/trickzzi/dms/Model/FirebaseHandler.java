package com.example.trickzzi.dms.Model;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.example.trickzzi.dms.JyabActivity.Payable;
import com.example.trickzzi.dms.JyabActivity.Receivable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
public class FirebaseHandler
{
    DatabaseReference databaseReference;
    ValueEventListener valueEventListener;
    private List<User> userslist = new ArrayList<>();
    private  List<Double> moneyarray = new ArrayList<Double>();
    private List<MoneyTransfer> moneytransfer = new ArrayList<>();

    public void addPayableOfVictim(String touser,String fromuser,double amount,String PayableofVicitim)
    {
        databaseReference=FirebaseDatabase.getInstance().
                getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");

        //Jis ney deny hai usky Payable may add karna !
        double totalpayable= Double.parseDouble(PayableofVicitim)+amount;
        databaseReference.child(fromuser).child("payable").setValue(""+totalpayable);
    }
    public void addRecieableOFRequester(String touser,double amount,String ReceiableofRequester)
    {
        databaseReference=FirebaseDatabase.getInstance().
                getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");

        //Jis ney request ki hai usky recieable may add karny hai.
        double newreceiable=Double.parseDouble(ReceiableofRequester)+amount;
        databaseReference.child(touser).child("receivable").setValue(Double.toString(newreceiable));
    }

    public void SubReceiableofRequester(String requester_id,double amount,String ReceivableOfRequester,String BalanceofRequester,final Context c)
    {
        databaseReference=FirebaseDatabase.getInstance().
                getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");
        //jis ko pay kiye hai usky receivable may subtract, or balance may add karna hai.

        double totalreceivable= Double.parseDouble(ReceivableOfRequester);
            double newrec=totalreceivable-amount;
            databaseReference.child(requester_id).child("receivable").setValue(""+newrec).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(c, ""+e.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        double balance=Double.parseDouble(BalanceofRequester);
        balance=balance+amount;
        databaseReference.child(requester_id).child("balance").setValue(""+balance).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(c, ""+e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void addCredit(String user_id,double amount,String BalanceofUser,Context c)
    {
        databaseReference=FirebaseDatabase.getInstance().
                getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");
        //jis ko pay kiye hai usky receivable may subtract, or balance may add karna hai.

        double balance=Double.parseDouble(BalanceofUser);
        balance=balance+amount;
        databaseReference.child(user_id).child("balance").setValue(""+balance);
        Toast.makeText(c,"Credited Sucessfully!",Toast.LENGTH_LONG).show();
    }
    public void withdrawMoney(String user_id,double amount,String BalanceofUser,Context c)
    {
        databaseReference=FirebaseDatabase.getInstance().
                getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");
        //jis ko pay kiye hai usky receivable may subtract, or balance may add karna hai.

        double balance=Double.parseDouble(BalanceofUser);
        if(balance>=amount)
        {
            balance=balance-amount;
            databaseReference.child(user_id).child("balance").setValue(""+balance);
            Toast.makeText(c,"WithDraw Money Sucessfully !",Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(c,"Low Balance !",Toast.LENGTH_LONG).show();


        }
    }

    public void Update(String user_id,String vicitim_id,double oldamt,double newamount,String ReceivableOfRequester, String PayableofVictim)
    {



        databaseReference=FirebaseDatabase.getInstance().
                getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");

            double payable= Double.parseDouble(PayableofVictim);
            payable=payable-oldamt;
            payable=payable+newamount;
            databaseReference.child(vicitim_id).child("payable").setValue(""+payable);


            double rec= Double.parseDouble(ReceivableOfRequester);
            rec=rec-oldamt+newamount;
            databaseReference.child(user_id).child("receivable").setValue(""+rec);



    }

    public void GetBalPayableReceiable(final String user_id, final TextView payable, final TextView recTExt)
    {

        Query q=FirebaseDatabase.getInstance().getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/").
                child("money_transfer").orderByChild("from_user_id").equalTo(user_id);
        valueEventListener=new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                double totalpayable=0;
                double bal=0;
                double rec=0;
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    MoneyTransfer mt_obj = obj.getValue(MoneyTransfer.class);
                    if (TextUtils.equals(mt_obj.getFrom_user_id(),user_id))
                    {
                        double payable = Double.parseDouble(mt_obj.getMt_amount());
                        totalpayable += payable;
                    }
                    if (TextUtils.equals(mt_obj.getTo_user_id(),user_id)) {
                        double receiable = Double.parseDouble(mt_obj.getMt_amount());
                        rec += receiable;
                    }
                }
                payable.setText(""+totalpayable);
                recTExt.setText(""+rec);
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        };
        q.addListenerForSingleValueEvent(valueEventListener);

    }

    public void Delete(String user_id,String vicitim_id,double amount,String ReceivableOfRequester, String PayableofVictim)
    {
        databaseReference=FirebaseDatabase.getInstance().
                getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");

        double payable= Double.parseDouble(PayableofVictim);
        payable=payable-amount;
        if(payable<0)
        {
            databaseReference.child(vicitim_id).child("payable").setValue("0");

        }
        else
            {
            databaseReference.child(vicitim_id).child("payable").setValue(""+payable);
        }


        double rec= Double.parseDouble(ReceivableOfRequester);
        rec=rec-amount;
        if(rec<0)
        {

        }
        databaseReference.child(user_id).child("receivable").setValue(""+rec);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    public static double round2(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }














    public void Pay(String touser, String fromuser, double amount, String payable, String Receiable, Context c,String balance)
    {
        databaseReference=FirebaseDatabase.getInstance().
                getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");

        if(Double.parseDouble(balance)>amount)
        {
            double newbalance=Double.parseDouble(balance)-amount;

            double newpay=Double.parseDouble(payable)-amount;
            double newrec=Double.parseDouble(Receiable)-amount;


           // databaseReference.child(touser).child("receivable").setValue(strpay);
            //databaseReference.child(fromuser).child("payable").setValue(Double.toString(rec));
            Toast.makeText(c, "Paid Sucessfully !", Toast.LENGTH_SHORT).show();


        }
        else
        {
            Toast.makeText(c, "Low Amount", Toast.LENGTH_SHORT).show();

        }
    }
}
