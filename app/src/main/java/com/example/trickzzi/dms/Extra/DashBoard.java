package com.example.trickzzi.dms.Extra;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.trickzzi.dms.JyabActivity.Payable;
import com.example.trickzzi.dms.JyabActivity.Receivable;
import com.example.trickzzi.dms.R;

public class DashBoard extends AppCompatActivity
{

    TextView bal,name;
    Button payable,rec;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        bal=findViewById(R.id.balance);
        name=findViewById(R.id.name);
        payable=findViewById(R.id.btnpayable);
        rec=findViewById(R.id.btnrec);

        Intent intent=getIntent();
        name.setText(intent.getStringExtra("fullname"));
        bal.setText(intent.getStringExtra("bal"));
        payable.setText(intent.getStringExtra("payable"));
        rec.setText(intent.getStringExtra("rec"));
        final String user_id=intent.getStringExtra("user_id");

        payable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DashBoard.this,Payable.class);
                intent.putExtra("user_id", user_id);
                startActivity(intent);

            }
        });
        rec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(DashBoard.this,Receivable.class);
                intent.putExtra("user_id", user_id);
                startActivity(intent);

            }
        });






    }
}
