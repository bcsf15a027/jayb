package com.example.trickzzi.dms.JyabActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.trickzzi.dms.Model.FirebaseHandler;
import com.example.trickzzi.dms.Model.MoneyTransfer;
import com.example.trickzzi.dms.Model.User;
import com.example.trickzzi.dms.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class UserDashBoard extends AppCompatActivity
{

    private TextView name, bal, payable, rec;
    private ImageView Logout,admin;
    private CardView CV_payable, CV_rec, CV_SendReq, CV_msg,CV_name;
    private FirebaseDatabase firebaseDatabase,firebaseDatabase2;
    private DatabaseReference databaseReference,databaseReference2;

    private List<User> userslist = new ArrayList<>();
    private ValueEventListener valueEventListener,valueEventListener2;
    private ArrayList<String> Userskeys = new ArrayList<>();
    private ArrayList<String> CheckedBox = new ArrayList<>();
    private ProgressDialog progressDialog,progressDialog2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_dash_board);

        name = findViewById(R.id.MD_name);
        Logout = findViewById(R.id.MD_Logout);
        admin=findViewById(R.id.Admin_btn);
        bal = findViewById(R.id.MD_TV_bal);
        payable = findViewById(R.id.MD_TV_payable);
        rec = findViewById(R.id.MD_TV_rec);
        CV_payable=findViewById(R.id.MD_CV_payable);
        CV_rec=findViewById(R.id.MD_CV_Rec);
        CV_SendReq=findViewById(R.id.MD_CV_sendrequest);
        CV_msg=findViewById(R.id.MD_CV_msg);
        CV_name=findViewById(R.id.MD_CV_name);
        bal.setText("");
        payable.setText("");
        rec.setText("");

        progressDialog = new ProgressDialog(UserDashBoard.this);
        progressDialog.setMessage("Loading");
        progressDialog.setCancelable(false);
        progressDialog.show();

        progressDialog2 = new ProgressDialog(UserDashBoard.this);
        progressDialog2.setMessage("Loading");
        progressDialog2.setCancelable(false);
        progressDialog2.show();

        SharedPreferences sharedPreferences=getSharedPreferences("SavePassword",MODE_PRIVATE);
        final String user_id = sharedPreferences.getString("LoggedInUserID","null");
        final String IsAdmin = sharedPreferences.getString("IsAdmin","false");

        if(TextUtils.equals(IsAdmin,"true"))
        {
            admin.setVisibility(View.VISIBLE);
            admin.setEnabled(true);
        }
        else
        {
            admin.setVisibility(View.INVISIBLE);
            admin.setEnabled(false);
        }

        Query q=FirebaseDatabase.getInstance().getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/").
                child("money_transfer").orderByChild("from_user_id").equalTo(user_id);
        valueEventListener2=new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {

            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        };
        q.addListenerForSingleValueEvent(valueEventListener2);



        firebaseDatabase2 = FirebaseDatabase.getInstance();
        databaseReference2 = firebaseDatabase2.getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/money_transfer");
        databaseReference2.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                double totalpayable=0;
                double receivable=0;
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    MoneyTransfer mt_obj = obj.getValue(MoneyTransfer.class);
                    if (TextUtils.equals(mt_obj.getFrom_user_id(),user_id))
                    {
                        double payable = Double.parseDouble(mt_obj.getMt_amount());
                        totalpayable += payable;
                    }
                    if (TextUtils.equals(mt_obj.getTo_user_id(),user_id))
                    {
                        double currreceiable = Double.parseDouble(mt_obj.getMt_amount());
                        receivable += currreceiable;
                    }
                }
                payable.setText(""+totalpayable);
                rec.setText(""+receivable);
                progressDialog.dismiss();
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");
        databaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                userslist.clear();
                float count = dataSnapshot.getChildrenCount();
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    User user = obj.getValue(User.class);
                    Userskeys.add(obj.getKey());
                    userslist.add(user);
                }
                UpdateBalance(user_id);
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

        Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                SharedPreferences sharedPreferences=getSharedPreferences("SavePassword",MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("LoggedInUserID","null");
                editor.putBoolean("LoggedIn",false);
                editor.putString("IsAdmin","false");
                editor.commit();
                Intent intent = new Intent(UserDashBoard.this, MainActivity.class);
                startActivity(intent);


            }
        });

        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserDashBoard.this, AdminPanal.class);
                startActivity(intent);
            }
        });
        CV_payable.setOnClickListener(
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(UserDashBoard.this, Payable.class);
                        intent.putExtra("user_id", user_id);
                        startActivity(intent);

                    }
                });
        CV_rec.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(UserDashBoard.this, Receivable.class);
                        intent.putExtra("user_id", user_id);
                        startActivity(intent);

                    }
                });
        CV_SendReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserDashBoard.this, Distributer.class);
                startActivity(intent);

            }
        });
        CV_name.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Intent intent = new Intent(UserDashBoard.this, ModernDashboard.class);
                //startActivity(intent);
            }
        });



    }
    public void UpdateBalance(final String User1)
    {
        Query q=FirebaseDatabase.getInstance().getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/").
                child("users").orderByChild("user_id").equalTo(User1);
        valueEventListener=new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    User userobj = obj.getValue(User.class);
                    name.setText(userobj.getFull_name());
                    bal.setText("Rs. "+round2(Double.parseDouble(userobj.getBalance()),2));
                    //payable.setText("Rs. "+round2(Double.parseDouble(userobj.getPayable()),2));
                    //rec.setText("Rs. "+round2(Double.parseDouble(userobj.getReceivable()),2));
                }
                progressDialog2.dismiss();
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Toast.makeText(UserDashBoard.this, "Error Occourd!",Toast.LENGTH_LONG).show();
            }
        };
        q.addListenerForSingleValueEvent(valueEventListener);
    }
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    public static double round2(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
