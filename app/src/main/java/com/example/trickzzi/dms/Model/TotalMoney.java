package com.example.trickzzi.dms.Model;

public class TotalMoney
{
    private Double total;
    private String user_id;
    private String name;
    public TotalMoney()
    {

    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTotal() {
        return total;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getName() {
        return name;
    }
}
