package com.example.trickzzi.dms.JyabActivity;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.text.TextUtils;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.widget.Button;
import android.widget.EditText;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.trickzzi.dms.Adapters.ReceivableAdapter;
import com.example.trickzzi.dms.Model.FirebaseHandler;
import com.example.trickzzi.dms.Model.MoneyTransfer;
import com.example.trickzzi.dms.Extra.MyDividerItemDecoration;
import com.example.trickzzi.dms.Model.User;
import com.example.trickzzi.dms.R;
import com.example.trickzzi.dms.Extra.RecyclerTouchListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

public class Receivable extends AppCompatActivity
{

    private List<MoneyTransfer> MoneyTransferList = new ArrayList<>();
    private List<User> userslist = new ArrayList<>();
    private Dialog myDialog;
    private ValueEventListener valueEventListener;
    private ArrayList<String> keys = new ArrayList<>();
    private ArrayList<String> DataReadFlags = new ArrayList<>();
    private ReceivableAdapter mAdapter;
    private ProgressDialog progressDialog,progressDialog2;
    private RecyclerView recyclerView;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private Button TotalReceivable;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receivable);
        TotalReceivable=findViewById(R.id.TotalReceivable);

        SharedPreferences sharedPreferences=getSharedPreferences("SavePassword",MODE_PRIVATE);
        final String user_id = sharedPreferences.getString("LoggedInUserID","null");

        myDialog = new Dialog(this);

        progressDialog = new ProgressDialog(Receivable.this);
        progressDialog.setMessage("Loading");
        progressDialog.setCancelable(false);
        progressDialog.show();

        progressDialog2 = new ProgressDialog(Receivable.this);
        progressDialog2.setMessage("Loading");
        progressDialog2.setCancelable(false);
        progressDialog2.show();


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");
        databaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {

                userslist.clear();
                //DataReadFlags.add(0,"false");

                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    User user = obj.getValue(User.class);
                    userslist.add(user);
                }
                progressDialog.dismiss();
                //DataReadFlags.add(0,"true");
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/money_transfer");
        databaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {

                MoneyTransferList.clear();
                //DataReadFlags.add(1,"false");
                float count = dataSnapshot.getChildrenCount();
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {

                    MoneyTransfer moneyTransfer = obj.getValue(MoneyTransfer.class);
                    if (TextUtils.equals(moneyTransfer.getTo_user_id(),user_id))
                    {
                        keys.add(obj.getKey());
                        MoneyTransferList.add(moneyTransfer);

                    }
                }
                progressDialog2.dismiss();
                //DataReadFlags.add(1,"true");

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
        /*if(TextUtils.equals(DataReadFlags.get(0),"true") &&TextUtils.equals(DataReadFlags.get(1),"true"))
        {
            progressDialog.dismiss();
        }*/

        recyclerView = findViewById(R.id.receivable_Recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(Receivable.this));
        mAdapter = new ReceivableAdapter(MoneyTransferList,userslist,Receivable.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
       // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);



        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position)
            {
                final MoneyTransfer moneyTransfer= MoneyTransferList.get(position);
                TextView txtclose;
                final EditText desc,amount;
                final EditText t1;
                Button btnFollow,update;
                myDialog.setContentView(R.layout.custompopup);
                myDialog.setCancelable(false);
                myDialog.setCanceledOnTouchOutside(false);
                txtclose =(TextView) myDialog.findViewById(R.id.txtclose);
                desc =(EditText) myDialog.findViewById(R.id.popup_desc);
                amount =(EditText) myDialog.findViewById(R.id.popup_amount);
                update=(Button)myDialog.findViewById(R.id.btnupdate);
                btnFollow = (Button) myDialog.findViewById(R.id.btnfollow);

                desc.setText(moneyTransfer.getMt_desc());
                amount.setText(moneyTransfer.getMt_amount());

                txtclose.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        myDialog.dismiss();
                    }
                });
                //final String input=t1.getText().toString();
                btnFollow.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(desc.getText().toString().isEmpty() || amount.getText().toString().isEmpty() || amount.getText().toString().equals("0"))
                        {
                            Toast.makeText(getApplicationContext(), "Incorrect Input", Toast.LENGTH_SHORT).show();
                        }
                        else {


                            if (!(TextUtils.equals(desc.getText().toString(), moneyTransfer.getMt_desc()) && TextUtils.equals(amount.getText().toString(), moneyTransfer.getMt_amount()))) {
                                databaseReference = FirebaseDatabase.getInstance().
                                        getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/money_transfer");
                                databaseReference.child(keys.get(position)).child("mt_desc").setValue(desc.getText().toString());
                                databaseReference.child(keys.get(position)).child("mt_amount").setValue(amount.getText().toString());
                                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd-MM-yyyy");
                                String currentDateandTime = sdf.format(new Date());
                                databaseReference.child(keys.get(position)).child("request_time").setValue(currentDateandTime).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        Toast.makeText(getApplicationContext(), "Update Sucessfully !", Toast.LENGTH_SHORT).show();
                                        myDialog.dismiss();

                                    }
                                });
                            } else {
                                Toast.makeText(getApplicationContext(), "Saved Sucessfully !", Toast.LENGTH_SHORT).show();
                                myDialog.dismiss();

                            }
                            //Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_SHORT).show();

                        }

                    }
                });





                update.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        databaseReference = FirebaseDatabase.getInstance().
                                getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/money_transfer");
                        databaseReference.child(keys.get(position)).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(getApplicationContext(), "Delete Sucessfully !", Toast.LENGTH_SHORT).show();
                                myDialog.dismiss();

                            }
                        });

                    }
                });

                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.show();
                //Toast.makeText(getApplicationContext(), moneyTransfer.getMt_desc() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position)
            {

                Toast.makeText(getApplicationContext(), "Long Press!", Toast.LENGTH_SHORT).show();
            }
        }));
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        mAdapter.notifyDataSetChanged();

        TotalReceivable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Receivable.this,TotalReceivable.class);
                startActivity(intent);
            }
        });

    }

}

