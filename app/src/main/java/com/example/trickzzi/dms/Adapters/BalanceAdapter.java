package com.example.trickzzi.dms.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.trickzzi.dms.Model.MoneyTransfer;
import com.example.trickzzi.dms.Model.User;
import com.example.trickzzi.dms.R;

import java.util.List;
public class BalanceAdapter  extends RecyclerView.Adapter<BalanceAdapter.MyViewHolder>
{

        Context context;
        private List<User> userslist;

        public class MyViewHolder extends RecyclerView.ViewHolder
        {
            public TextView name,amount;
            public MyViewHolder(View view)
            {
                super(view);
                name = (TextView) view.findViewById(R.id.name_total_payable_lisview);
                amount = (TextView) view.findViewById(R.id.amt_total_payable_lisview);


            }

        }


    public BalanceAdapter(List<User> moviesList, Context c) {
        this.userslist = moviesList;
        this.context = c;
    }

    @Override
    public BalanceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.total_payable_listiview, parent, false);

        return new BalanceAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BalanceAdapter.MyViewHolder holder, int position)
    {
        User obj = userslist.get(position);
        holder.amount.setText("Rs. "+obj.getBalance());
        holder.name.setText(obj.getFull_name());
    }

    @Override
    public int getItemCount() {
        return userslist.size();

    }
}
