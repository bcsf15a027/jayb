package com.example.trickzzi.dms.JyabActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.trickzzi.dms.Adapters.BalanceAdapter;
import com.example.trickzzi.dms.Adapters.PayableTotalAdapter;
import com.example.trickzzi.dms.Extra.MyDividerItemDecoration;
import com.example.trickzzi.dms.Model.FirebaseHandler;
import com.example.trickzzi.dms.Model.User;
import com.example.trickzzi.dms.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AdminPanal extends AppCompatActivity
{
        private Button request_bal,withdraw;
        private ProgressBar progressBar;
        private EditText ammount;
        private CheckBox checkBox;
        private RecyclerView Balance_RV;
        private BalanceAdapter balanceAdapter;
        private FirebaseDatabase firebaseDatabase;
        private DatabaseReference databaseReference, dref;
        private List<User> userslist = new ArrayList<>();
        private List<String> SelectedUser = new ArrayList<>();
        private ArrayList<String> Userskeys = new ArrayList<>();
        private Spinner categorySpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_panal);
        Balance_RV=findViewById(R.id.balance_RV);
        ammount=findViewById(R.id.credit_amount);

        request_bal=findViewById(R.id.balance_request_btn);
        withdraw=findViewById(R.id.btn_withdraw);

        progressBar=findViewById(R.id.progress_circle);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userslist.clear();
                float count = dataSnapshot.getChildrenCount();
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    User user = obj.getValue(User.class);
                    Userskeys.add(obj.getKey());
                    userslist.add(user);
                }
                populateSpinnerCategory();
                registerSpinnerListener();
                progressBar.setVisibility(View.INVISIBLE);
                balanceAdapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
        Balance_RV.setLayoutManager(new LinearLayoutManager(AdminPanal.this));
        balanceAdapter= new BalanceAdapter(userslist,this);
        RecyclerView.LayoutManager mTLayoutManager = new LinearLayoutManager(getApplicationContext());
        Balance_RV.setLayoutManager(mTLayoutManager);
        Balance_RV.setItemAnimator(new DefaultItemAnimator());
        Balance_RV.setAdapter(balanceAdapter);
        Balance_RV.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        balanceAdapter.notifyDataSetChanged();

        request_bal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                FirebaseHandler firebaseHandler=new FirebaseHandler();
                if(SelectedUser.size()!=0)
                {
                    int index=Integer.parseInt(SelectedUser.get(0))-1;
                    firebaseHandler.addCredit(SelectedUser.get(0), Double.parseDouble(ammount.getText().toString()),userslist.get(index).getBalance(),AdminPanal.this);
                }
                else
                {
                    Toast.makeText(AdminPanal.this,"Select a User !",Toast.LENGTH_LONG).show();

                }



            }
        });
        withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                FirebaseHandler firebaseHandler=new FirebaseHandler();
                if(SelectedUser.size()!=0)
                {
                    int index=Integer.parseInt(SelectedUser.get(0))-1;
                    firebaseHandler.withdrawMoney(SelectedUser.get(0), Double.parseDouble(ammount.getText().toString()),userslist.get(index).getBalance(),AdminPanal.this);
                }
                else
                {
                    Toast.makeText(AdminPanal.this,"Select a User !",Toast.LENGTH_LONG).show();

                }

            }
        });

    }




        private void registerSpinnerListener()
        {
            categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
                {
                    if(adapterView.getItemAtPosition(i).equals("Select User"))
                    {
                        SelectedUser.clear();
                    }
                    else
                        {
                            SelectedUser.clear();
                        String category = adapterView.getItemAtPosition(i).toString();
                        SelectedUser.add(""+i);
                        //Toast.makeText(AdminPanal.this, "selected : "+category, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }

        private void populateSpinnerCategory()
        {
            categorySpinner = findViewById(R.id.categorySpinner);
            List<String> USER = new ArrayList<>();
            USER.add(0,"Select User");
            for(int i=0;i<userslist.size();i++)
            {
                USER.add(userslist.get(i).getFull_name());
            }

            ArrayAdapter<String> dataAdapter;
            dataAdapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,USER);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            categorySpinner.setAdapter(dataAdapter);
        }
}
