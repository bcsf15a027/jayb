package com.example.trickzzi.dms.JyabActivity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.trickzzi.dms.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class StartActivity extends AppCompatActivity
{
    private TextInputLayout name,password,email;
    private FirebaseAuth mAuth;
    private Button Register;
    @Override
    public void onStart()
    {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null)
        {
            Intent intent=new Intent(StartActivity.this,MainActivity.class);
            startActivity(intent);
            finish();

        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        mAuth = FirebaseAuth.getInstance();

        name=(TextInputLayout) findViewById(R.id.start_Reg_name);
        password=(TextInputLayout) findViewById(R.id.start_Reg_password);
        email=(TextInputLayout) findViewById(R.id.start_Reg_email);
        Register=findViewById(R.id.start_reg_btn);


        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                mAuth.createUserWithEmailAndPassword(email.getEditText().getText().toString(),password.getEditText().getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                            Toast.makeText(getApplicationContext(),"User Register Sucessfully !",Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(StartActivity.this,MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else
                            {
                                Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();


                        }
                    }
                });


            }
        });
    }

}
