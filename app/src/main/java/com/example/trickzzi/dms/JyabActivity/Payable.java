package com.example.trickzzi.dms.JyabActivity;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.example.trickzzi.dms.Model.TotalMoney;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.example.trickzzi.dms.Adapters.PayableAdapter;
import com.example.trickzzi.dms.Adapters.PayableTotalAdapter;
import com.example.trickzzi.dms.Model.FirebaseHandler;
import com.example.trickzzi.dms.Model.MoneyTransfer;
import com.example.trickzzi.dms.Extra.MyDividerItemDecoration;
import com.example.trickzzi.dms.Model.User;
import com.example.trickzzi.dms.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import android.widget.EditText;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import com.example.trickzzi.dms.Extra.RecyclerTouchListener;




public class Payable extends AppCompatActivity
{
    private List<MoneyTransfer> MoneyTransferList = new ArrayList<>();
    private List<User> userslist = new ArrayList<>();
    private Button Totalpayable;
    private Dialog myDialog;
    private ArrayList<String> keys = new ArrayList<>();
    private PayableAdapter mAdapter;
    private ProgressDialog progressDialog,progressDialog2;
    private RecyclerView recyclerView;

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private FirebaseDatabase firebaseDatabase2;
    private DatabaseReference databaseReference2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payable);
        myDialog = new Dialog(this);
        SharedPreferences sharedPreferences=getSharedPreferences("SavePassword",MODE_PRIVATE);
        final String user_id = sharedPreferences.getString("LoggedInUserID","null");
        Totalpayable=findViewById(R.id.TotalPayable);

        progressDialog = new ProgressDialog(Payable.this);
        progressDialog.setMessage("Loading");
        progressDialog.setCancelable(false);
        progressDialog.show();

        progressDialog2 = new ProgressDialog(Payable.this);
        progressDialog2.setMessage("Loading");
        progressDialog2.setCancelable(false);
        progressDialog2.show();


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/users");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userslist.clear();
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    User user = obj.getValue(User.class);
                    userslist.add(user);
                }
                progressDialog2.dismiss();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        firebaseDatabase2 = FirebaseDatabase.getInstance();
        databaseReference2 = firebaseDatabase2.getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/money_transfer");
        databaseReference2.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                int i = 0;
                MoneyTransferList.clear();
                float count = dataSnapshot.getChildrenCount();
                for (DataSnapshot obj : dataSnapshot.getChildren())
                {
                    MoneyTransfer moneyTransfer = obj.getValue(MoneyTransfer.class);
                    if(moneyTransfer.getFrom_user_id().equals(user_id))
                    {
                        keys.add(obj.getKey());
                        MoneyTransferList.add(moneyTransfer);
                    }
                }
                progressDialog.dismiss();
                mAdapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
        recyclerView = findViewById(R.id.payable_Recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(Payable.this));
        mAdapter = new PayableAdapter(MoneyTransferList,userslist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);




        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position)
            {
                final MoneyTransfer moneyTransfer= MoneyTransferList.get(position);
                TextView pop_desc,pop_amount,txtclose;
                final EditText objection;
                Button btn_payable_pop_pay,btn_payable_pop_object;

                myDialog.setContentView(R.layout.popup_payable);
                myDialog.setCancelable(false);
                myDialog.setCanceledOnTouchOutside(false);
                pop_desc =(TextView) myDialog.findViewById(R.id.popup_desc_txt);
                pop_amount =(TextView) myDialog.findViewById(R.id.popup_amount_txt);
                txtclose =(TextView) myDialog.findViewById(R.id.txtclose);
                objection =(EditText) myDialog.findViewById(R.id.edittxt_objection);
                btn_payable_pop_pay=(Button)myDialog.findViewById(R.id.popup_pay_bnt);
                btn_payable_pop_object = (Button) myDialog.findViewById(R.id.popup_pay_objection);

                pop_desc.setText(moneyTransfer.getMt_desc());
                pop_amount.setText(moneyTransfer.getMt_amount());

                txtclose.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        myDialog.dismiss();
                    }
                });

                //final String input=t1.getText().toString();
                btn_payable_pop_pay.setOnClickListener(
                        new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        double amount = Double.parseDouble(moneyTransfer.getMt_amount());
                        int payerId = Integer.parseInt(user_id) - 1;
                        int RequesterID = Integer.parseInt(moneyTransfer.getTo_user_id()) - 1;
                        double balance = Double.parseDouble(userslist.get(payerId).getBalance());
                        double balance2=0;
                        if (balance >= amount)
                        {
                            balance = balance - amount;
                            databaseReference.child(user_id).child("balance").setValue("" + balance).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getApplicationContext(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                            });

                            balance2 = Double.parseDouble(userslist.get(RequesterID).getBalance());
                            balance2 = balance2 + amount;
                            databaseReference.child(moneyTransfer.getTo_user_id()).child("balance").setValue(""+balance2).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getApplicationContext(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                            });

                            databaseReference = FirebaseDatabase.getInstance().
                                    getReferenceFromUrl("https://dmsapp-f0da9.firebaseio.com/money_transfer");
                            databaseReference.child(keys.get(position)).setValue(null).addOnCompleteListener(
                                    new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            Toast.makeText(getApplicationContext(), "Paid Sucessfully !", Toast.LENGTH_SHORT).show();
                                            myDialog.dismiss();


                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getApplicationContext(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                            });
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Recharge Your Account Thanks !", Toast.LENGTH_SHORT).show();


                        }
                    }

                    });




                btn_payable_pop_object.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {

                    }
                });
                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.show();

                //Toast.makeText(getApplicationContext(), moneyTransfer.getMt_desc() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position)
            {

                Toast.makeText(getApplicationContext(), "Long Press!", Toast.LENGTH_SHORT).show();
            }
        }));
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        mAdapter.notifyDataSetChanged();
        Totalpayable.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(Payable.this,TotalPayableReceivable.class);
                startActivity(intent);
            }
        });

    }

}

